<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Кабинет</title>
</head>
<body>
<table align="center" width="100%" cellpadding="10">
    <thead style="background-color: #F0FFFF">
    <th align="left" style="text-decoration-color: #B0E0E6">
        <h1 style="color: #B0E0E6">Task Manager</h1>
    </th>
    <th align="right">
        <h4 align="right" style="color: darkcyan"><a href="/logout">Выйти</a></h4>
    </th>
    </thead>
</table>
<hr align="left" width="100%" color="#2F4F4F" />
<h3 align="left">Пользователь: ${pageContext.request.userPrincipal.name}</h3>
<h3 style="color: #B0E0E6"> | Кабинет</h3>
<hr align="left" width="300" size="3" color="#B0E0E6" />
<br>
<div align="left">
    <a href="/tm/tasks">Задачи</a>
    <br>
    <a href="/tm/newtask">Добавить задачу</a>
    <br>

    <sec:authorize access="hasRole('ADMIN')">
        <a href="/admin">Пользователи</a>
        <br>
    </sec:authorize>
</div>
<br>
<div>
    <a href="/">Главная</a>
</div>

</body>
</html>
