<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>All users</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
</head>

<body>
<h3 style="color: #B0E0E6"> | All users </h3>
<div align="center">
    <br>
    <table align="center" border="1" cellspacing="0" cellpadding="10" width="90%" height="90">
        <thead style="background-color: #B0E0E6">
        <th>ID</th>
        <th>UserName</th>
        <th>Password</th>
        <th>Roles</th>
        <th></th>
        </thead>
        <c:forEach items="${userList}" var="user">
            <tr style="background-color: #F0FFFF">
                <td>${user.id}</td>
                <td>${user.username}</td>
                <td>${user.password}</td>
                <td>
                    <c:forEach items="${user.roles}" var="role">${role.name}; </c:forEach>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/admin" method="post">
                        <input type="hidden" name="userId" value="${user.id}"/>
                        <input type="hidden" name="action" value="delete"/>
                        <button type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
    <a href="/tm">back</a>
</div>
</body>
</html>