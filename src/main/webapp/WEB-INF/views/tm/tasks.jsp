<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Задачи</title>
</head>
<body>
<table align="center" width="100%" cellpadding="10">
    <thead style="background-color: #F0FFFF">
    <th align="left" style="text-decoration-color: #B0E0E6">
        <h1 style="color: #B0E0E6">Task Manager</h1>
    </th>
    <th align="right">
        <h4 align="right" style="color: darkcyan"><a href="/logout">Выйти</a></h4>
    </th>
    </thead>
</table>
<hr align="left" width="100%" color="#2F4F4F" />
<h3 align="left">Пользователь: ${pageContext.request.userPrincipal.name}</h3>
<h3 style="color: #B0E0E6"> | Задачи</h3>
<hr align="left" width="300" size="3" color="#B0E0E6" />
<br>

<div align="center">
    <table border="1" cellspacing="0" cellpadding="3" width="70%">
        <thead style="background-color: #B0E0E6">
        <th>ID</th>
        <th>Задача</th>
        <th>Описание</th>
        <th></th>
        </thead>
        <c:forEach items="${taskList}" var="task">
            <c:if test="${pageContext.request.userPrincipal.name == task.user.username}">
                <tr style="background-color: #F0FFFF">
                    <td>${task.id}</td>
                    <td>${task.name}</td>
                    <td>${task.description}</td>
                    <td>
                        <a href="/tm/edit?id=${task.id}">Изменить</a>
                    </td>
                    <td>
                        <form action="${pageContext.request.contextPath}/tm/tasks" method="post">
                            <input type="hidden" name="taskId" value="${task.id}"/>
                            <input type="hidden" name="action" value="delete"/>
                            <button type="submit">Удалить</button>
                        </form>
                    </td>
                </tr>
            </c:if>
        </c:forEach>
    </table>

</div>
<br>
<div align="center">
    <a href="/tm">назад</a>
</div>
</body>
</html>
