<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Новая задача</title>
</head>
<body>
<table align="center" width="100%" cellpadding="10">
    <thead style="background-color: #F0FFFF">
    <th align="left">
        <h1 style="color: #B0E0E6">Task Manager</h1>
    </th>
    <th align="right">
        <h4 align="right" style="color: darkcyan"><a href="/logout">Выйти</a></h4>
    </th>
    </thead>
</table>
<hr align="left" width="100%" color="#2F4F4F" />
<h3 align="left">Пользователь: ${pageContext.request.userPrincipal.name}</h3>
<h3 style="color: #B0E0E6"> | Новая задача</h3>
<hr align="left" width="300" size="3" color="#B0E0E6" />
<br>

<%--@elvariable id="task" type="java"--%>
<form:form method="post" modelAttribute="task" action="${pageContext.request.contextPath}/tm/newtask">

    <div align="center">
        <h3>Добавить задачу:</h3>
        <form:input type="text" path="name" placeholder="Наименование"
                    autofocus="true"></form:input>
        <form:errors path="name"></form:errors>
            ${nameError}
    </div>
    <div align="center">
        <form:input type="description" path="description" placeholder="Описание"></form:input>
        <input type="hidden" name="username" value="${pageContext.request.userPrincipal.name}"/>

    </div>
    <div align="center">
        <button type="submit">Добавить</button>
    </div>

</form:form>
<br>
<div align="center">
    <a href="/tm">назад</a>
</div>
</body>
</html>
