<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<html>
<head>
    <title>Изменение задачи</title>
</head>
<body>
<table align="center" width="100%" cellpadding="10">
    <thead style="background-color: #F0FFFF">
    <th align="left">
        <h1 style="color: #B0E0E6">Task Manager</h1>
    </th>
    </thead>
</table>
<div>
    <form method="POST" action="${pageContext.request.contextPath}/tm/edit" >
        <h2 align="center">Изменение задачи</h2>
        <div align="center">
            <input align="center" name="name" type="text" placeholder="Новое наименование" autofocus="true"/>
            <br>
            <input align="center" name="description" type="text" placeholder="Новое описание"/>
            <input type="hidden" name="id" value="${taskForm.id}">
            <input type="hidden" name="username" value="${pageContext.request.userPrincipal.name}"/>
            <br>
            <button type="submit">Изменить</button>
        </div>
    </form>
</div>
<br>
<div align="center">
    <a href="/tm/tasks">назад</a>
</div>
</body>
</html>
