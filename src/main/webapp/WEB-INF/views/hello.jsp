<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css">
</head>
<body>
<table align="center" width="100%" cellpadding="10">
    <thead>
    <th align="center" style="text-decoration-color: #2F4F4F">
        <h1 style="color: #2F4F4F">  Task Manager</h1>
        <hr width="100%" size="3" color="#B0E0E6" />
    </th>
    </thead>
</table>
<div align="center">
    <sec:authorize access="!isAuthenticated()">
        <h3><a href="/login">Войти</a></h3>
        <h5><a href="/registration">Создать аккаунт</a></h5>
    </sec:authorize>
    <sec:authorize access="isAuthenticated()">
        <div>
            <a href="/tm"><h3>${pageContext.request.userPrincipal.name}</h3></a>
            <br>
            <h4><a href="/logout">Выйти</a></h4>
        </div>

    </sec:authorize>
    <hr width="100%" size="2" color="#B0E0E6" />
</div>
</body>
</html>