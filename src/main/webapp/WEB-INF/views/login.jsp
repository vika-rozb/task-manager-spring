<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Вход</title>
</head>

<body>
<table align="center" width="100%" cellpadding="10">
    <thead style="background-color: #F0FFFF">
    <th align="left">
        <h1 style="color: #B0E0E6">Task Manager</h1>
    </th>
    </thead>
</table>
<sec:authorize access="isAuthenticated()">
    <% response.sendRedirect("/"); %>
</sec:authorize>
<div>
    <form method="POST" action="/login">
        <h2 align="center">Вход в систему</h2>
        <div align="center">
            <input align="center" name="username" type="text" placeholder="Имя пользователя"
                   autofocus="true"/>
            <br>
            <input align="center" name="password" type="password" placeholder="Пароль"/>
            <br>
            <button type="submit">Войти</button>
            <h5><a href="/registration">Создать аккаунт</a></h5>
        </div>
    </form>
</div>

</body>
</html>