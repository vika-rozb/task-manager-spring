package com.ejvk.tm.controllers;

import com.ejvk.tm.entity.User;
import com.ejvk.tm.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping()
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        return "registration";
    }

    @PostMapping("/registration")
    public String createUser(@ModelAttribute("userForm") @Valid User userForm,
                             BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        if(!userForm.getPassword().equals(userForm.getPasswordConfirm())) {
            model.addAttribute("passError", "Password mismatch");
            return "registration";
        }
        if(!userService.saveUser(userForm)) {
            model.addAttribute("usernameError", "This user name is already taken. Please use different one.");
            return "registration";
        }
        return "redirect:/";
    }

}
