package com.ejvk.tm.controllers;

import com.ejvk.tm.entity.Task;
import com.ejvk.tm.services.TaskService;
import com.ejvk.tm.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/tm")
public class TaskController {
    @Autowired
    private TaskService taskService;
    @Autowired
    private UserService userService;

    @GetMapping("/tasks")
    public String look(Model model) {
        model.addAttribute("taskList", taskService.findAllTasks());
        return "/tm/tasks";
    }
    @PostMapping("/tasks")
    public String deleteTask(@RequestParam() Long taskId,
                             @RequestParam() String action) {
        if (action.equals("delete")) {
            taskService.delete(taskId);
        }
        return "redirect:/tm/tasks";
    }

    @GetMapping("/newtask")
    public String newTask(Model model) {
        model.addAttribute("task", new Task());
        return "tm/newtask";
    }

    @PostMapping("/newtask")
    public String create(@ModelAttribute("task") @Valid Task task,
                         BindingResult bindingResult,
                         @RequestParam() String username) {
        if (bindingResult.hasErrors()) {
            return "redirect:/tm/newtask";
        }
        task.setUser(userService.findUserByUsername(username));
        if (!taskService.saveTask(task)) {
            return "redirect:/tm/newtask";
        }
        return "redirect:/tm";
    }

    @GetMapping("/edit")
    public String edit(@RequestParam(value = "id") int taskId, Model model) throws Exception {
        model.addAttribute("taskForm", taskService.findTaskById((long)taskId));
        return "tm/edit";
    }

    @PostMapping("/edit")
    public String updateTask(@ModelAttribute("taskForm") @Valid Task task, BindingResult bindingResult,
                             @RequestParam(value = "id") int id,
                             @RequestParam(value = "username") String username) throws Exception {
        if (bindingResult.hasErrors()) {
            return "/tm/edit";
        }
        task.setUser(userService.findUserByUsername(username));
        if ((taskService.findTaskById((long)id) != null) && taskService.saveTask(task)) {
            return "redirect:/tm/tasks";
        }
        return "/tm/edit";
    }
}
