package com.ejvk.tm.services;

import com.ejvk.tm.entity.Role;
import com.ejvk.tm.entity.User;
import com.ejvk.tm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findUserByUsername(username);

        Set<GrantedAuthority> grantedAuthority = new HashSet<>();
        for (Role role : user.getRoles()) {
            grantedAuthority.add(new SimpleGrantedAuthority(role.getName()));
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthority);
    }

     public User findUserById(Long userId) throws Exception {
        User userBD = userRepository.getById(userId);

        if (userBD == null) {
            throw new Exception("User not found");
        }
        return userBD;
     }

     public User findUserByUsername(String username) throws UsernameNotFoundException{
         User user = userRepository.findByUsername(username);
         if (user == null) {
             throw new UsernameNotFoundException("User not found");
         }
         return user;
     }

     public List<User> allUsers() {
        return userRepository.findAll();
     }

     public boolean saveUser(User user) {
        User userBD = userRepository.findByUsername(user.getUsername());
        if (userBD != null) {
            return false;
        }

        user.setRoles(Collections.singleton(new Role(1L, "ROLE_USER")));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return true;
     }

     public boolean deleteUser(Long userId) {
        if (userRepository.findById(userId).isPresent()) {
            userRepository.deleteById(userId);
            return true;
        }
        return false;
     }
}
