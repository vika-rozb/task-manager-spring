package com.ejvk.tm.services;

import com.ejvk.tm.entity.Task;
import com.ejvk.tm.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {
    @Autowired
    private TaskRepository taskRepository;

    public boolean saveTask(Task task) {

        taskRepository.save(task);
        return true;
    }

    public Task findTaskById(Long taskId) throws Exception {
        Task taskDB =  taskRepository.getById(taskId);
        if (taskDB == null) {
            throw new Exception("Task not found");
        }
        return taskDB;
    }

    public List<Task> findAllTasks() {
        return taskRepository.findAll();
    }

    public boolean delete(Long taskId) {
        if (taskRepository.findById(taskId).isPresent()) {
            taskRepository.deleteById(taskId);
            return true;
        }
        return false;
    }
}
