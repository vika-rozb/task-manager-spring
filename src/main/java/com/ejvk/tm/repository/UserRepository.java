package com.ejvk.tm.repository;

import com.ejvk.tm.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {
    @Query("SELECT n FROM User n WHERE n.username = :username")
    User findByUsername(@Param("username") String username);
}
